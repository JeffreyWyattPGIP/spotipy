# This script allows interaction with the Spotify API to control playback on the local machine
# It includes speech recognition for some functionality


# Importing the spotipy library and the needed modules from the library
import spotipy
import speech_recognition as r
import pyaudio
from spotipy.oauth2 import SpotifyOAuth

recognizer = r.Recognizer()

# Assigning the Client ID, Client Secret, and Redirect URL
# All these values will change based upon your specific spotify dev settings
client_id = "9d95486d1b6844808abd5140cb9a46fe"
client_secret = "024ab362e5cc47a5a72d893a0a318a48"
# Redirect URI must match the application value in developer settings
redirect_uri = "https://fresh-asf.com/"

# Creates an object with the assigned values
auth_var = spotipy.Spotify(auth_manager=SpotifyOAuth(client_id=client_id,
                                                     client_secret=client_secret,
                                                     redirect_uri=redirect_uri))

# It may be necessary to configure a scope
# scope="user-modify-playback-state user-read-playback-state user-library-read playlist-modify-public
# playlist-modify-private"


# Function to add track to playlist
# Input parameter is the name of the track


def add_song_to_playlist(track):
    print("\nList of Playlists")
    # Store playlist values in list
    list_playlists = auth_var.current_user_playlists()["items"]
    # Display available options contained in list_playlists
    for x in range(len(list_playlists)):
        print(str(i+1)+". ", list_playlists[x]['name'])
    # Prompt for playlist to append
    num = int(input("\nWhat number playlist would you like to add the song to?\n"))
    # This is not necessary
    # playlist_uri = ""
    # If the value is within an acceptable range
    if 0 < num < len(list_playlists)+1:
        # Set the playlist URI
        playlist_uri = list_playlists[num-1]['id']
        # Append the selected track URI
        auth_var.playlist_add_items(playlist_id=playlist_uri, items=[track])
        # Display confirmation
        print("Song successfully added to playlist named "+list_playlists[num-1]['name']+"!\n")
    else:
        # Display input validation error
        print("Invalid number entered\n")


# Function to play selected song


def play_song():
    # Asks the user to input the name of the song they want to play
    # song = input("Enter song: \n")
    with r.Microphone() as mic_source:
        print("Enter song: ")
        audio_val = recognizer.listen(mic_source)
        song = recognizer.recognize_google(audio_val)

    # The search method returns a dictionary where there are keys within named "tracks".
    # Items are the keys within the dictionary which point to a track or list of tracks.
    # The 0 refers to the first element in the track list, which there is only 1 anyway since limit = 1.
    # Lastly, "uri" retrieves the Spotify URI of the track we just searched for.
    searched_song = auth_var.search(q=song, limit=1, type="track")["tracks"]["items"][0]["uri"]

    # Executes the method to start playing the song within the local Spotify app
    # and prompt for adding it to the playlist
    auth_var.start_playback(uris=[searched_song])
    temp = input("Would you like to add this song to one of your playlists? (y/n)\n")
    if temp == "y" or temp == "Y":
        add_song_to_playlist(searched_song)
    elif temp == "n" or temp == "N":
        print("No worries, enjoy the song!\n")
    else:
        # Defaults to input validation error
        print("Invalid input")


# Play the local playlist
# Input parameters are the URI of the list and list of existing playlists
# playlist_name is not used


def play_local_playlist(playlist_name, num, playlists):
    # Initialize shuffle check to false
    shuffle_check = False
    # Prompt for shuffling tracks y/n
    answer = input("Do you want to shuffle the playlist? (y/n)\n")
    if answer == "y" or answer == "Y":
        shuffle_check = True
    elif answer == "n" or answer == "N":
        shuffle_check = False
    else:
        # Defaults to no shuffling
        print("Invalid input. Playing playlist without shuffling the songs!\n")
    # Object method to set shuffle
    auth_var.shuffle(shuffle_check)
    # Start the local playlist
    auth_var.start_playback(context_uri=playlists[num]['uri'])


# Function to play public playlist
# Input is name of playlist


def play_public_playlist(playlist_name):
    # Discover URI value for playlist and store in uri_val
    uri_val = auth_var.search(q=playlist_name, limit=1, type="playlist")["playlists"]['items'][0]["uri"]
    # Prompt for shuffling tracks y/n
    shuffle_value = input("Do you want to play the playlist shuffled or not? (y/n)\n")
    # This variable is not used
    # check = False
    if shuffle_value == "y":
        # Object method to set shuffle to true
        auth_var.shuffle(True)
        # Start the public playlist
        auth_var.start_playback(context_uri=uri_val)
    elif shuffle_value == "n":
        # Start the public playlist without shuffle
        auth_var.start_playback(context_uri=uri_val)
    else:
        # Defaults to no shuffling
        print("Input was invalid, so playing playlists without shuffle feature!\n")
        # Start the public playlist
        auth_var.start_playback(context_uri=uri_val)


# Function to display artist and tracks query
# Input parameter is the track


def show_artist_and_tracks(val):
    search_val = auth_var.search(q=val, limit=1, type="artist")
    artist_name = search_val['artists']['items'][0]
    total_top_tracks = auth_var.artist_top_tracks(artist_name['id'])['tracks']

    for n in range(5):
        print(str(n+1)+". "+str(total_top_tracks[n]['name']))
    at_ans = input("\nDo you want to play a song? (y/n)\n")
    if at_ans == "y":
        num = int(input("Choose the number corresponding to either of the 5 songs: \n"))
        if 0 < num < 6:
            auth_var.start_playback(uris=[total_top_tracks[num-1]['uri']])
        else:
            print("Invalid input.\n")
    elif at_ans == "n":
        print("No worries, I'll take you back to the main menu!\n")
    else:
        print("Invalid input.\n")


# Menu for the user
while True:
    print("\nMenu\n1) Play song\n2) Play playlist\n3) Search for an artist\n4) Exit")
    # selection = input("Please speak your choice 1,2,3, or 4!\n")
    with r.Microphone() as source:
        print("Which option would you like: (say: option x)")
        audio = recognizer.listen(source)
        selection = recognizer.recognize_google(audio)
    if selection == "option one":
        play_song()
    elif selection == "option two":
        selection = input("Do you want to play a local playlist or a public one?\nType either \"local\" or"
                          " \"public\"\n")
        if selection == "local":
            playlist_list = auth_var.current_user_playlists()['items']
            for i in range(len(playlist_list)):
                print(str(i+1)+". "+playlist_list[i]['name'])
            ans = int(input("Choose the corresponding number for the playlist you would like to play: \n"))
            name = playlist_list[ans-1]
            if 0 < ans < len(playlist_list)+1:
                play_local_playlist(name, ans-1, playlist_list)
            else:
                print("Input is invalid! Let me take you back to the main menu.\n")
        elif selection == "public":
            name = input("What public playlist do you want to play?\n")
            play_public_playlist(name)
        else:
            print("Invalid input! Let me take you back to the main menu.\n")
    elif selection == "option three":
        name = input("What artist do you want to search for?\n")
        show_artist_and_tracks(name)
    elif selection == "option four":
        print("Thanks for using our music player :)\n")
        break
    else:
        print("Invalid input, please make another choice!\n")
